import os
from kit.dict import Dict

if os.environ.get('env') == 'prod':
    from .prod import *
else:
    from .dev import *

RMQ = Dict(RMQ)
