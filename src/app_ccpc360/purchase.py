# -*- coding: utf-8 -*-
import re
from typing import List
from urllib.parse import urljoin

import httpx
from parsel import Selector
from kit.tool import date_time

from .exceptions import NeedLoginError


class Purchase:
    """
    https://www2.ccpc360.com/f/purchase.php?fid=1
    """

    def __init__(self, cookies=None, headers=None):

        self.keyword = None
        self.page = 1
        self.session = httpx.Client(headers=headers, cookies=cookies)

    def crawl_list(self, keyword=None, page=None, start_time=None, end_time=None):
        """
        抓取列表页
        :param keyword: 关键词
        :param page: 页码
        :param start_time: 开始时间, 默认为当前时间之前一个月
        :param end_time: 结束时间, 默认为当前时间
        :return:
        """
        assert (keyword or self.keyword) is not None, "keyword is None"
        params = {
            'fid': '1',
            'page': page or self.page,
            'keyword': keyword or self.keyword,
            'pushStime': start_time or date_time.get_before_time(30, 'days', '%Y-%m-%d'),
            'pushEtime': end_time or date_time.get_now('%Y-%m-%d'),
        }

        response = self.session.get('https://www2.ccpc360.com/f/purchase.php',
                                    params=params)
        if response.status_code != 200:
            raise NeedLoginError(f'Failed to crawl purchase list, response status code: {response.status_code}')
        parser = Selector(response.text)
        purchase_list: List[str] = parser.xpath("//tbody/tr/td[@class='text-left']/div/a/@href").getall()
        return purchase_list

    def crawl_detail(self, url: str):
        if not url.startswith('http'):
            url = urljoin('https://www2.ccpc360.com/f/', url)
        pid = re.search(r'id=(\d+)', url).group(1)
        response = self.session.get(url)

        parser = Selector(response.text)
        extractor_detail = self._extractor(parser)
        return {
            'id': pid,
            **extractor_detail
        }

    def get_tel(self, pid: str, elId: str, type: str = '1'):
        url = 'https://www2.ccpc360.com/f/purchasedetailGetTel.php'
        data = {
            'id': pid,
            'elId': elId,
            'type': type,
        }
        response = self.session.post(url, data=data)
        return response.json().get('tel')

    @staticmethod
    def _parse_contact(contacts):
        """解析联系人"""
        rules = {
            'company': r'采购：(.*?)\|',
            'contact': r'联系人：(.*?)\|',
            'address': r'地址：(.*?)\|',
            'head': r'是否负责采购：(.*?)$',
        }
        contact = {}
        contact_str = '|'.join(contacts)
        for k, v in rules.items():
            o = re.search(v, contact_str)
            contact[k] = o.group(1) if o else None
        return contact

    def _extractor(self, parser: Selector):
        region = parser.xpath("//th[text()='所属地区']/following-sibling::td[1]/text()").get()
        publish_date = parser.xpath("//th[text()='发布时间']/following-sibling::td[1]/text()").get()
        purchase_date = parser.xpath("//th[text()='采购日期']/following-sibling::td[1]/text()").get()
        demand = parser.xpath("//th[text()='采购需求']/following-sibling::td[1]/text()").get()
        demand = demand.strip() if demand else None
        contacts = parser.xpath("//div[contains(@class, 'contacts')]//text()").getall()
        contacts = [i.strip() for i in contacts if i.strip()]

        tel_params = parser.xpath("//a[text()='点击查看手机号' or text()='点击查看电话']/@onclick").get()
        tel_params = tel_params.split('(')[1].split(')')[0].split(',')
        tel_params = [i.strip() for i in tel_params]  # ['30990', '52503', '1']
        tel = self.get_tel(*tel_params)

        return {
            'region': region,
            'publish_date': publish_date,
            'purchase_date': purchase_date,
            'demand': demand,
            'tel': tel,
            **self._parse_contact(contacts)
        }
