# -*- coding: utf-8 -*-
import time

from kit.rpc.middleware import Middleware


class DelayMiddleware(Middleware):

    def before_process_message(self, broker, message, *args, **kwargs):
        time.sleep(5)