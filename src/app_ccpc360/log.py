# -*- coding: utf-8 -*-
from loguru import logger


def register_log():
    return logger.bind(app_name="ccpc360")


LOGGER = register_log()
