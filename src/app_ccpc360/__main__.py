# -*- coding: utf-8 -*-
from kit.rpc import rpc
from kit.rpc.store import set_store
from kit.store.rabbitmq import RabbitmqStore
from .config import RMQ
from .log import LOGGER

store = RabbitmqStore(
    host=RMQ.host,
    port=RMQ.port,
    username=RMQ.username,
    password=RMQ.password,
    ssl=RMQ.ssl
)
set_store(store)


def main():
    from .job.purchase_list import job_ccpc_purchase_list
    from .job.purchase_detail import job_ccpc_purchase_detail
    LOGGER.warning('启动成功')
    rpc.run_forever()


if __name__ == '__main__':
    main()
