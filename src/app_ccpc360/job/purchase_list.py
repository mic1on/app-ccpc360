# -*- coding: utf-8 -*-
from itertools import product
from kit.rpc import rpc
from kit.rpc.broker import Broker

from ..exceptions import NeedLoginError
from ..middleware.unique import UniqueMiddleware
from ..purchase import Purchase
from ..log import LOGGER

from_purchase_list = Broker('job.ccpc.purchase.list')
to_purchase_detail = Broker('job.ccpc.purchase.detail',
                            middlewares=[UniqueMiddleware()])


@rpc.job(from_broker=from_purchase_list, to_broker=to_purchase_detail)
def job_ccpc_purchase_list(message, *args, **kwargs):
    rpc.state.cookies = message.kwargs.cookies
    rpc.state.headers = message.kwargs.headers
    assert rpc.state.cookies and rpc.state.headers, \
        "cookies and headers can't be empty"

    keywords = message.kwargs.keywords or []
    page = message.kwargs.page or 1
    purchase = Purchase(rpc.state.cookies, rpc.state.headers)

    kps = list(product(keywords, range(1, page + 1)))
    for kw, p in kps:
        LOGGER.info('start crawl purchase list', extra={'keyword': kw, 'page': p})
        try:
            purchase_list = purchase.crawl_list(kw, p)
            purchase_list = [{**message, 'url': item} for item in purchase_list]
            LOGGER.info('end crawl purchase list', extra={'keyword': kw, 'page': p, 'count': len(purchase_list)})
        except NeedLoginError:
            # TODO: 发送事件通知
            return None
        except Exception as e:
            LOGGER.error(f'Failed to crawl purchase list: {e}')
            return None
        yield from purchase_list
