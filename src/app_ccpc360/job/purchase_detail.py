# -*- coding: utf-8 -*-
from kit.rpc import rpc
from kit.rpc.broker import Broker

from ..middleware.delay import DelayMiddleware
from ..purchase import Purchase
from ..log import LOGGER


from_purchase_detail = Broker('job.ccpc.purchase.detail', middlewares=[DelayMiddleware()])
# to_purchase_result = Broker('job.ccpc.purchase.result')
to_purchase_result = Broker('bidding_forecast')


@rpc.job(from_broker=from_purchase_detail, to_broker=to_purchase_result)
def job_ccpc_purchase_detail(message, *args, **kwargs):
    LOGGER.info('start crawl purchase detail', extra={'url': message.url})
    purchase = Purchase(message.kwargs.cookies, message.kwargs.headers)
    try:
        purchase_detail = purchase.crawl_detail(message.url)
        LOGGER.info('end crawl purchase detail', extra={**purchase_detail})
    except Exception as e:
        LOGGER.error(f'Failed to crawl purchase detail: {e}')
        return None
    return purchase_detail


