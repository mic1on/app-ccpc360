# -*- coding: utf-8 -*-

cookies = {
    'PHPSESSID': 'o848rv7sskucpge0h7sjm663r0',
    'passport': '193166%09zdbyq2022%09BlMGBAYFA1BQUgkEAlABA1EEBw5RBVRQAFpaDwRaUAc%3D97bb697381',
    'acw_tc': '0b6e705216659717229204665e01428d9077c002b24722c3d025e5733ff4a5',
}

headers = {
    'authority': 'www2.ccpc360.com',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'accept-language': 'zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
    'cache-control': 'no-cache',
    'content-type': 'application/x-www-form-urlencoded',
    # Requests sorts cookies= alphabetically
    # 'cookie': 'USR=gyo0q605%091%091665212424%097c8c02bc25606779d4d5b295c3f5f6e3; Qs_lvt_372065=1664327732; Qs_pv_372065=2194307202198045200%2C2560847993912431600%2C2811498166316636700%2C3668684975041703000%2C3150872104485500400; Qs_lvt_469985=1659678423%2C1664239257%2C1664327705%2C1665192402; Qs_pv_469985=2794997591315339300%2C562393411297211460%2C120898765923350130%2C2723872630131505000%2C1036935890820669600; PHPSESSID=s8q1m0i07k2fvvg4aqcdud5gs4; passport=193166%09zdbyq2022%09BlMGBAYFA1BQUgkEAlABA1EEBw5RBVRQAFpaDwRaUAc%3D97bb697381; USR=gyo0q605%091%091665210067%09b15ed0f37c7c8849fe93a11412d9774d; acw_tc=0b6e704216652118736663117e012593a49689e4a1ba37ae20e41ed22b91f0; SERVERID=2a0871a9c2ff0077d050e7aaabcee98b|1665212425|1665192405',
    'origin': 'https://www2.ccpc360.com',
    'pragma': 'no-cache',
    'referer': 'https://www2.ccpc360.com/f/search.php?remark=nzj',
    'sec-ch-ua': '"Chromium";v="106", "Microsoft Edge";v="106", "Not;A=Brand";v="99"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"macOS"',
    'sec-fetch-dest': 'document',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-user': '?1',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36 Edg/106.0.1370.34',
}
#
# RMQ = {
#     "host": "testing.ceegdev.com",
#     "port": 5672,
#     "ssl": False,
#     "username": "admin",
#     "password": "qweQWE123!@#",
#     "virtual_host": "/",
#     "job_queue": "job",
#     "honey_queue": "honey",
#     "scraper_queue": "honey",
#     "data_clean_queue_from": "honey",
#     "data_clean_queue_to": "bidding",
#     "data_rate_queue_from": "AI",
# }

RMQ = {
    "host": "b-60def1c8-fb70-41af-8ed6-3ae896bb9831.mq.cn-northwest-1.amazonaws.com.cn",
    "port": 5671,
    "ssl": True,
    "username": "admin",
    "password": "Li=qYsaIg=KIc%d?2n5eGc6AZn4T!rkR",
    "virtual_host": "/"
}
